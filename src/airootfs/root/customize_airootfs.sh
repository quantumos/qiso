#!/bin/bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
chmod 700 /root
chown root /etc

sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

# Enable stuff on boot
systemctl enable pacman-init.service choose-mirror.service
systemctl enable NetworkManager
systemctl set-default graphical.target
systemctl disable sddm
systemctl enable sddm-plymouth

# Add live user (password live)
useradd live
echo "root:live" | chpasswd
echo "live:live" | chpasswd
chown -R live:live /home/live

# Give live user nopass sudo
echo "live ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# Install RTL88XX wifi drivers (common in wifi dongles)
# This is OK to do on the live ISO because it has to support a wide range of software
# but not OK to do when actually installing the OS because it will probably
# be adding bloat to a system that doesn't need it
pacman-key --init
pacman-key --populate archlinux

cd /home/live
sudo -u live git clone https://aur.archlinux.org/yay.git
cd yay
yes | sudo -u live makepkg -si
cd ..
rm -rf yay

yes | sudo -u live yay -S --nocleanmenu --nodiffmenu --noeditmenu --noupgrademenu --removemake --cleanafter rtl88xxau-aircrack-dkms-git rtl88x2bu-dkms-git tela-icon-theme-git plymouth-git

# Rebuild initrd with plymouth
echo "HOOKS=(base udev plymouth memdisk archiso_shutdown archiso archiso_loop_mnt archiso_pxe_common archiso_pxe_nbd archiso_pxe_http archiso_pxe_nfs archiso_kms block filesystems keyboard)" > /etc/mkinitcpio.conf
echo 'COMPRESSION="xz"' >> /etc/mkinitcpio.conf
mkinitcpio -p linux

# Install calamares
git clone https://github.com/calamares/calamares
cd calamares
cmake .
make
make install
mkdir -p /home/live/Desktop
cp calamares.desktop /home/live/Desktop
chown -R live:live /home/live/Desktop
cd ..
rm -rf calamares
pacman -R extra-cmake-modules cmake
